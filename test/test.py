import json
from ml4proflow import modules, modules_extra
dfm = modules.DataFlowManager()
dfg = modules.DataFlowGraph(dfm, {})
m1 = modules_extra.RepeatingSourceModule(dfm, {})
dfg.add_module(m1)
with open('config.json', 'w') as jf:
    json.dump(dfg.get_config(), jf)
