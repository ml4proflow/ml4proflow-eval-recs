import os
import unittest
from ml4proflow_eval_recs import ml4proflow_eval_recs_cli

""" # Code to generate test config:
import json
from ml4proflow import modules, modules_extra
dfm = modules.DataFlowManager()
dfg = modules.DataFlowGraph(dfm, {})
m1 = modules_extra.RepeatingSourceModule(dfm, {})
dfg.add_module(m1)
with open('config.json', 'w') as jf:
    json.dump(dfg.get_config(), jf)
"""


class TestModulesModule(unittest.TestCase):
    def test_create_new_sink_module(self):
        print(os.getcwd())
        args = ['--config-glob', 'config.json', '--runs', '2', '--recs-nodes-ssh-user', 'test', '--recs-nodes-ssh-pass', 'test', '--override-arch', 'x86_64']
        nodes = [('127.0.0.1', 'docker', None)]
        ml4proflow_eval_recs_cli.main(args, nodes)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
