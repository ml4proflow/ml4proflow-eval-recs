from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="ml4proflow-eval-recs",
    version="0.0.1",
    author="Christian Klarhorst",
    author_email="cklarhor@techfak.uni-bielefeld.de",
    description="Evaluation utilities",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="todo",
    project_urls={
        "Main framework": "todo",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    install_requires=["ml4proflow",
                      "requests",
                      "paramiko",
                      "python-gssapi",
                      "xmltodict"
                      ],
    extras_require={
        "tests": ["pytest", 
                  "pandas-stubs",
                  "types-paramiko",
                  "types-requests",
                  "pytest-html",
                  "pytest-cov",
                  "flake8",
                  "jinja2==3.0.3", # for flake8?
                  "mypy"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    python_requires=">=3.6", # todo
)
