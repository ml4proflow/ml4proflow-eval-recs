#!/usr/bin/env python3
import argparse
from datetime import datetime
import json
import time
from os.path import relpath, basename
import logging
import os
import sys
from glob import iglob


def run(configs, args, nodes):
    from ml4proflow_eval_recs.ssh import SSHRunner, RecsMgmt, NvidiaPower
    for ip, host, recs_node in nodes:
        logging.info("[%s] Connecting", host)
        test = SSHRunner(ip,
                         username=args.recs_nodes_ssh_user,
                         password=args.recs_nodes_ssh_pass,
                         conda_version=args.target_conda_version,
                         gss_auth=args.recs_nodes_ssh_gss_auth,
                         override_arch=args.override_arch,
                         key_filename=args.recs_key_filename,
                         )
        if args.clean_gits:
            test.clean_gits()
        if args.clean_conda:
            test.clean_conda()
        test.create_root_path()
        test.install_conda()
        test.install_main_framework()
        test.install_extra_gits()
        if recs_node:
            recs_node.start_record("%s/%s-energy.csv" % (args.output_directory,
                                                         host))
        if args.sleep_before:
            logging.info("[%s] Sleeping", host)
            time.sleep(args.sleep_before)
        for config in configs:
            logging.info("[%s] run next config (%s)", host, basename(config))
            start_time = time.time()
            run_output_file = "%s/%s-%s.json" % (args.output_directory,
                                                 host,
                                                 basename(config).split('.')[0])
            run_config = str(config)
            data = test.run_config(run_config, args.runs)
            end_time = time.time()
            final_run_data = {'config': config,
                              'runs': args.runs,
                              'host': host,
                              'host_start_time': start_time,
                              'host_end_time': end_time,
                              'data': data
                              }
            with open(run_output_file, "w") as f:
                json.dump(final_run_data, f)
        if args.sleep_after:
            logging.info("[%s] Sleeping", host)
            time.sleep(args.sleep_after)
        if recs_node:
            recs_node.stop()
        logging.info("[%s] Closing", host)
        test.close()


def main(arguments: list[str] = sys.argv[1:], override_nodes=None) -> None:
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M")
    parser = argparse.ArgumentParser(
        description='Run processing pipelines on recs nodes')
    parser.add_argument('--clean-gits', action='store_true')
    parser.add_argument('--clean-conda', action='store_true')
    parser.add_argument('--config-glob', type=str)
    parser.add_argument('--config-prefix', type=str, default=os.getcwd())
    parser.add_argument('--output-directory', type=str,
                        default=f'./run-{timestamp}/')
    parser.add_argument('--target-conda-version', type=str, default='4.9.2')
    parser.add_argument('--recs-key-filename', default=None)
    parser.add_argument('--recs-mgmt-url', type=str,
                        default='http://192.168.11.50:80/REST')
    parser.add_argument('--recs-mgmt-user', type=str, default='admin')
    parser.add_argument('--recs-mgmt-pass', type=str, default='admin')
    parser.add_argument('--recs-nodes-ssh-user', type=str,
                        default=os.environ.get('USER', None))
    parser.add_argument('--recs-nodes-ssh-pass', type=str,
                        default=None)
    parser.add_argument('--recs-nodes-ssh-gss_auth', default=False)
    parser.add_argument('--runs', type=int, default=1000)
    parser.add_argument('--sleep-before', type=int, default=0)
    parser.add_argument('--sleep-after', type=int, default=0)
    parser.add_argument('--override-arch', type=str, default='')
    parser.add_argument('--log-file', type=str, default=f'{timestamp}.log')
    parser.add_argument('--log-lvl', default=logging.INFO,
                        choices=[logging.DEBUG, logging.INFO, logging.WARN])
    args = parser.parse_args(arguments)
    logging.basicConfig(filename=args.log_file, level=args.log_lvl)
    logging.getLogger().addHandler(logging.StreamHandler(sys.stderr))
    os.makedirs(args.output_directory)
    recs_config = {'base_url': args.recs_mgmt_url,
                   'user': args.recs_mgmt_user,
                   'password': args.recs_mgmt_pass
                   }
    from ml4proflow_eval_recs.ssh import SSHRunner, RecsMgmt, NvidiaPower
    # RECS Nodes (IP, Name, RECS_MGMT_NAME)
    nodes = [('192.168.11.101', 'node_101',
              RecsMgmt(dict(recs_config, node_id='RCU_86656893236_BB_1_0'))),
             #('192.168.11.102', 'node_102',
             # NvidiaPower('192.168.11.102', args.recs_nodes_ssh_user)),
             ('192.168.11.103', 'node_103',
              RecsMgmt(dict(recs_config, node_id='RCU_86656893236_BB_3_0'))),
             ('192.168.11.104', 'node_104',
              RecsMgmt(dict(recs_config, node_id='RCU_86656893236_BB_4_0'))),
             ('192.168.11.106', 'node_106',
              RecsMgmt(dict(recs_config, node_id='RCU_86656893236_BB_6_0'))),
             ('192.168.11.155', 'node_155',
              NvidiaPower('192.168.11.155', args.recs_nodes_ssh_user)),
             ('192.168.11.156', 'node_156',
              NvidiaPower('192.168.11.156', args.recs_nodes_ssh_user)),
             ('192.168.11.159', 'node_159',
              NvidiaPower('192.168.11.159', args.recs_nodes_ssh_user)),
             ]
    if override_nodes:
        nodes = override_nodes
    configs = [relpath(f, args.config_prefix) for f in iglob(args.config_glob)]
    logging.info("Found %s configs!", len(configs))
    run(configs, args, nodes)


if __name__ == '__main__':
    main(sys.argv[1:])
