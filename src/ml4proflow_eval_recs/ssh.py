import paramiko
import requests
import xmltodict
import pandas
import time
import multiprocessing
import logging
# we just assume utf-8 everywhere

logger = logging.getLogger(__name__)

class SSHRunner:
    def __init__(self, host: str, conda_version: str, port: int = 22,
                 username: str = None, password: str = None,
                 key_filename: str = None, gss_auth: bool = False,
                 root_path='~/ml4proflow-gits/', override_arch: str = ''):
        self.host = host
        self.port = port
        self.conda_version = conda_version
        self.root_path = root_path

        # paramiko.common.logging.basicConfig(level=paramiko.common.INFO)
        if username is None or username == '':
            logger.warning('[%s] Warning username is not set', host)
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(host, port, username=username, password=password,
                         key_filename=key_filename, gss_auth=gss_auth)
        self.arch: str = override_arch
        self.conda_shell_setup = f'eval "$(~/miniconda3-{conda_version}/bin/conda shell.bash hook)"'
        self.conda_activate_env = f'{self.conda_shell_setup};conda activate ml4proflow-standalone/local_files'
        self.cmd_prefixes = f'export GIT_SSL_NO_VERIFY=1; cd {root_path}'

    def _run_cmd(self, cmd: str) -> str:
        stdin, stdout, stderr = self.ssh.exec_command(cmd)
        stdin.flush()
        stdin.channel.shutdown_write()
        if stdout.channel.recv_exit_status() != 0:
            logger.info('[%s] EXITCODE!=0 (ok if no exception is raised)',
                         host)
            logger.warning('[%s][STDOUT]%s', self.host, stdout.read())
            logger.warning('[%s][STDERR]%s', self.host, stderr.read())
            raise Exception("EXITCODE!=0")
        result = []
        buf = stdout.read()
        while len(buf) > 0:
            result.append(buf.decode('utf-8'))
            buf = stdout.read()
        return "".join(result)

    def _extract_git_dirname(self, git_url: str) -> str:
        # extract target git directory name
        return git_url.split('/')[-1][:-4]

    def _install_git(self, git_url: str) -> None:
        git_name = self._extract_git_dirname(git_url)
        self._run_cmd(f'{self.cmd_prefixes}; git clone {git_url} || (cd {git_name}; git reset --hard; git pull)')

    def clean_gits(self) -> None:
        logger.info("[%s] Clean gits", self.host)
        self._run_cmd(f'rm -rf {self.root_path}')

    def clean_conda(self) -> None:
        self._run_cmd('rm -rf ~/miniconda*')

    def create_root_path(self) -> None:
        self._run_cmd(f'mkdir -p {self.root_path}')

    def install_main_framework(self, install_script: str = 'env-install-basic.cmd') -> None:
        logger.info("[%s] Installing framework", self.host)
        self._install_git('https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-standalone.git')
        try:
            self._run_cmd(f'{self.cmd_prefixes};{self.conda_shell_setup}; cd ml4proflow-standalone; ./{install_script}')
        except:
            pass

    def install_extra_gits(self):
        logger.info("[%s] Installing extra gits", self.host)
        extra_gits = [{'url': 'https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-eval-recs.git', 'cmds': []},
                      {'url': 'https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-pub.git', 'cmds': []}]
        for git in extra_gits:
            dir_name = self._extract_git_dirname(git['url'])
            self._install_git(git['url'])
            for cmd in git.get('cmds', []):
                self._run_cmd(f'{self.cmd_prefixes};{self.conda_activate_env};cd {dir_name};{cmd}')

    def probe_arch(self) -> str:
        if self.arch == '':
            self.arch = self._run_cmd("uname -i").strip()
        return self.arch

    def install_conda(self) -> None:
        try:
            self._run_cmd(f'{self.conda_shell_setup};which conda')
            # conda is already installed
            return
        except Exception:
            pass
        arch = self.probe_arch()
        conda_url = f'https://repo.anaconda.com/miniconda/Miniconda3-py39_{self.conda_version}-Linux-{arch}.sh'
        self._run_cmd(f'{self.cmd_prefixes}; wget {conda_url} -O conda_install.sh')
        self._run_cmd(f'{self.cmd_prefixes}; chmod +x conda_install.sh && '
                      + f'./conda_install.sh -bf -p ~/miniconda3-{self.conda_version}')
        self._run_cmd(f'{self.conda_shell_setup};conda config --set auto_activate_base false')

    def run_config(self, config: str, runs: int, extra_args: str = '') -> dict:
        args = f'--graph-desc {config} --process-n-times {runs} --record-times {extra_args}'
        cmd = f'{self.cmd_prefixes};{self.conda_activate_env};cd ml4proflow-pub; ml4proflow-cli {args}'
        raw = self._run_cmd(cmd)
        data = raw.strip().split("\n")
        result: dict[str, list[str]] = {}
        for d in data:
            if len(d) == 0:
                logger.error("[%s] CMD result is empty!!!", host)
                logger.info("[%s] CMD was '%s'", host, cmd)
                continue
            try:
                k, v = d.split(':', 1)
            except:
                logger.error("[%s] CMD result was not parsable!!!", host)
                logger.error("[%s] Expected a key/value pair in '%s'",
                              host, d)
                logger.info("[%s] Full CMD result was '%s'", host, data)
            tmp = result.get(k, [])
            tmp.append(v)
            result[k] = tmp
        return result

    def close(self):
        self.ssh.close()


def recs_mgmt_worker(recs_mgmt, target_file):
    data = {}
    while recs_mgmt.queue.empty():
        data[time.time()] = recs_mgmt.get_node_data()
        time.sleep(0.25)
    d = pandas.DataFrame.from_dict(data, orient='index')
    d.to_csv(target_file)


def nvidia_mgmt_worker(nvidia_mgmt, target_file):
    ssh = SSHRunner(nvidia_mgmt.host, '', username=nvidia_mgmt.username)
    data = {}
    stdin, stdout, stderr = ssh.ssh.exec_command("tegrastats --interval 200")
    while nvidia_mgmt.queue.empty():
        buf = stdout.readline()
        data[time.time()] = buf
    ssh.close()
    d = pandas.DataFrame.from_dict(data, orient='index')
    d.to_csv(target_file)


class Mgmt():
    def start_record(self, target_file: str):
        pass

    def stop(self):
        pass


class NvidiaPower(Mgmt):
    def __init__(self, host, username):
        self.host = host
        self.username = username
        self.running = False

    def start_record(self, target_file: str):
        if self.running:
            raise Exception("Already running")
        self.queue: multiprocessing.SimpleQueue = multiprocessing.SimpleQueue()
        self.proc = multiprocessing.Process(target=nvidia_mgmt_worker,
                                            args=(self, target_file,))
        self.proc.start()

    def stop(self):
        self.queue.put(False)


class RecsMgmt(Mgmt):
    def __init__(self, config: dict):
        self.base_url = config['base_url']  # http://localhost:20008/REST/
        self.node_id = config['node_id']  # RCU_86656893236_BB_1_0
        self.user = config['user']
        self.password = config['password']
        self.session = requests.Session()
        self.session.auth = (self.user, self.password)
        self.running = False

    def get_node_data(self):
        data = self.session.get(f'{self.base_url}/node/{self.node_id}').content
        return xmltodict.parse(data,
                               attr_prefix='',
                               cdata_key='',
                               dict_constructor=dict)['node']

    def start_record(self, target_file: str):
        if self.running:
            raise Exception("Already running")
        self.queue: multiprocessing.SimpleQueue = multiprocessing.SimpleQueue()
        self.proc = multiprocessing.Process(target=recs_mgmt_worker,
                                            args=(self, target_file,))
        self.proc.start()

    def stop(self):
        self.queue.put(False)
